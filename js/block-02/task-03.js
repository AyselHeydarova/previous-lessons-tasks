/* TASK - 3
* Create a countdown function.
* Arguments:
*   startPoint - start of countdown, It needs to be a String formatted 'HH:MM:SS' only
*   separator - this is the separator for showing the countdown on the page. Can be a String with only one character in it. Example: "/"
* */