/* TASK - 3
* Rewrite TASK-2.
* Ask user the amount of rectangles that will be created and the background color for all of them.
* Create a CSS class with the basic properties: width: 200px; height: 200px; margin: 5px
*
* Ask the number of the rectangles
* Ask the background color
* Create a loop, where inside the each iteration:
*   create a rectangle
*   add class created in CSS
*   add background color
*   append the element on the page BEFORE the first element with the tag script
*/
