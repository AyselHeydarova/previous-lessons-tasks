/* TASK - 5
* Create a function, that will ask user what exact item he wants to chenge.
* User need to write only item name, If there is no matches of user enter in the list
* keep asking until item name will be valid.
* Then ask a new amount of selected item.
* Show data, entered by user in the page.
* Example:
* user entered item name - 'Coffee',
* than entered new amount of it - '17'
* It means that you need to change only amount of 'Coffee' and it needs to be 17 now.
* */

const listOfItems = {
    Apple : 15,
    Orange: 0,
    Banana: 21,
    Pear: 0,
    Pineapple: 3,
};

let userItem = prompt('Enter exact item you want to change');

while (!listOfItems[userItem]) {
    userItem = prompt("Item is not valid. Please enter valid item");
}

let newAmount = prompt('Enter new amount of items');

listOfItems[userItem] = +newAmount;

console.log(listOfItems);


