/*TASK - 18
* Write a function that will return some random word from the entered string.
* Place textarea, 'Ok' button and an empty span on the page.
* Maxlength of the input string is 958 characters.
* 'Ok' button should be inactive until textarea will have at least one symbol in it.
* After the click on the 'Ok' button - random word from the string
* entered in the textarea needs to be sown as a text inside the span.
* You need to have some pull of already selected words to be sure that your selection has no duplicates.
* */
