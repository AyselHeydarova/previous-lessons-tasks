/* TASK - 2
* Create a function, that will:
* take an element from the page with class 'training-list', and text content equals 'list-element 5'.
* Show this element in console.
* Replace text content in this element to "<p>Hello</p>" without creating a new HTML element on the page
* Use array methods to complete the task.
* */

let listElement = document.getElementsByClassName('training-list');

 for (let i = 0; i < listElement.length; i++) {
     if (listElement[i].textContent === 'list-element 5') {
         console.log(listElement[i]);
         listElement[i].textContent = "<p>Hello</p>";
     }
}

// const listElements = document.querySelector('.training-list');
//  listElements.forEach(() => {
//
//  });
