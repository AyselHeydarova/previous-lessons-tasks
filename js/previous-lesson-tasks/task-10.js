/* TASK - 10:
* Create a chess desk. Div container for it has to ba on the HTML by default and has id='chess'.
* Create a class for basic styles of the desk cells: display:inline-block; width:150pxl height:150px; margin:0;
* User set the size of the desk and the two colors for it.
* Ask the size of the desk - it has to be a even number, so add the correctness checking
* Change the size of the desk container, that will fit the amount of cells.
* Ask the color for "white" cells
* Ask the color for "black" cells - it can't be the same, add the checking for this.
* Create a document fragment
* Create a loop, inside the each iteration you will:
*   create a rectangle
*   add the class with basic js properties
*   add the correct background color for this particular cell
*   append this rectangle into a document fragment
* After the loop:
*   append the document fragment as a child element inside the chess desk container
* */
