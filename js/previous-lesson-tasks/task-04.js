/* TASK - 4
* There is a list of items on the user screen.
* Create a function that will find the 'run out' items, amount of them is equal to 0.
* Replace the 0 inside text content of those elements to 'run out' and change text color to red.
*/


const items = document.querySelectorAll('.item');

let newList = '';

for (let i =0; i<items.length; i++) {
    if (items[i].textContent.includes('0')) {
        newList = items[i].textContent.replace('0', 'run-out');
        items[i].style.color = 'red';
    } else {
        newList = items[i].textContent;
    }
    console.log(newList);
}












