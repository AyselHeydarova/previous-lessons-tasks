/*TASK - 14
* Create a function, that will be changing the background color of the 100px square randomly,
* by the clicking on it. Every color should be random, transparent and white are not included in the list of colors.
 */
const  randomColor = function () {
  let red = parseInt(Math.random() * 250);
  let green = parseInt(Math.random() * 250);
  let blue = parseInt(Math.random() * 250);
  let opacity = Math.random();
  return `rgba(${red}, ${green}, ${blue}, ${opacity})`;
};
