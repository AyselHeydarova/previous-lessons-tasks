/* TASK - 3
* Get an element with class 'remove-me' and remove it from the page.
* Find element with class 'make-me-bigger'. Replace class 'make-me-bigger' to 'active'.
* Class 'active' already exists in CSS.
* */


const remove = document.querySelector('.remove-me');

remove.remove();

const makeBigger = document.querySelector('.make-me-bigger');

makeBigger.classList.replace('make-me-bigger','active');




