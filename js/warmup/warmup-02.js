/*   W A R M U P
* Bring some life to your packman from CSS animation lesson.
*
* Features to improve:
*   1 - movement control by pressing keyboard arrows
*   2 - stop the packman, if it's close to the end of the screen from any side
*   3 - add three modes to this game by giving user the ability to change the distance for one click. Packman can go 25px, 50px, 100px for one move. Changing the modes should be happening after pressing any 2 keyboard buttons that you choose (example - Ctrl+1, Ctrl+2, Ctrl+3).
*   4 - Add start screen with button 'start' and title and no any other content on the page. After click on 'start' - show the packman with all functionality.
*/

function parsePosition(transformVal) {
  if (transformVal === '') {
    return {
      left: 0,
      top: 0
    }
  } else {
    return {
      left: parseInt(transformVal.substring(
        transformVal.indexOf('(') + 1,
        transformVal.indexOf('px,')
      )),
      top: parseInt(transformVal.substring(
        transformVal.indexOf('px,') + 4,
        transformVal.indexOf(')') - 2
      ))
    }
  }
}

document.body.addEventListener('keydown', function (event) {
  const {key} = event,
    packman = document.querySelector('.packman'),
    step = 25,
    position = parsePosition(packman.style.transform),
    {innerWidth, outerHeight} = window;

  switch (key) {
    case 'ArrowRight':
      if (position.left + packman.offsetWidth + 25 < innerWidth) {
        packman.style.transform = `translate(${position.left + step}px, ${position.top}px) rotate(0deg)`;
      }
      break;
    case 'ArrowLeft':
      if (position.left - 15 > 0) {
        packman.style.transform = `translate(${position.left - step}px, ${position.top}px) rotate(0deg) scale(-1,1)`;
      }
      break;
    case 'ArrowUp':
      if (position.top - 15 > 0) {
        packman.style.transform = `translate(${position.left}px, ${position.top - step}px) rotate(-90deg)`;
      }
      break;
    case 'ArrowDown':
      if (position.top + packman.offsetHeight < outerHeight) {
        packman.style.transform = `translate(${position.left}px, ${position.top + step}px) rotate(90deg)`;
      }
      break;
  }
});