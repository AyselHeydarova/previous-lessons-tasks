/*   W A R M U P
* Write a function for showing the random words from input string.
* UI - input[type=text], okay button, empty span
*
* All UI elements should be created and placed on the page using JS.
*
* The task is:
* User can enter a string with any words in it. All the words should be separated with ' '.
* Okay button is disabled until at least 1 character entered in input.
* After click by okay button - random word from input string should be sowed up in a span.
* Random words that was showed needs to be saved in some pull, because your task is to show different random words.
* If all words from input string was successfully showed - okay button becomes disabled, text inside span becomes red.
* */

function RandomString(containerSelector, spliterator) {
  this.spliterator = spliterator;
  this.elements = {
    container: document.querySelector(containerSelector)
  };
  this.renderUI();
  this.assignEvents();
}

RandomString.prototype.renderUI = function () {
  let {elements} = this;
  const enterText = document.createElement('input'),
    okBtn = document.createElement('button'),
    msgText = document.createElement('span');

  enterText.classList.add('random-str-input');
  okBtn.classList.add('random-str-btn');
  msgText.classList.add('random-str-msg');

  okBtn.textContent = 'Ok';
  okBtn.disabled = true;

  elements.container.append(
    enterText,
    okBtn,
    msgText
  );

  this.elements = {
    ...elements,
    enterText,
    okBtn,
    msgText
  }
};

RandomString.prototype.assignEvents = function () {
  const {elements, spliterator} = this;

  elements.enterText.addEventListener('keyup', function (e) {
    elements.okBtn.disabled = e.target.value.length <= 1;
  });

  let allWords = [],
    isFirstClick = true;

  elements.okBtn.addEventListener('click', function () {
    elements.enterText.disabled = true;

    if(isFirstClick) {
      elements.msgText.style.color = 'black';
      allWords = elements.enterText.value.split(spliterator);
      isFirstClick = false;
    }

    const randomIndex = Math.round(Math.random() * allWords.length-1),
      toShow = allWords.splice(randomIndex, 1);
    // debugger
    if (allWords.length === 0) {
      elements.enterText.disabled = false;
      elements.enterText.value = '';
      elements.msgText.style.color = 'red';
      elements.msgText.textContent = `${toShow}. End of the input string`;
      elements.okBtn.disabled = true;
      isFirstClick = true;
    } else {
      elements.msgText.textContent = toShow;
    }
  });
};

const r = new RandomString('.random-container', ' ');
